__kernel void vector_add(__global ulong *src, __global ulong *sub_sum, ulong 
		num_elements) {

	int gid = get_global_id(0);
	
#if 0
	int offset = gid << 5;
	sub_sum[gid] = 
		src[offset] + src[offset+1] + src[offset+2] + src[offset+3] + 
		src[offset+4] + src[offset+5] + src[offset+6] + src[offset+7] + 
		src[offset+8] + src[offset+9] + src[offset+10] + src[offset+11] + 
		src[offset+12] + src[offset+13] + src[offset+14] + src[offset+15] +
		src[offset+16] + src[offset+17] + src[offset+18] + src[offset+19] + 
		src[offset+20] + src[offset+21] + src[offset+22] + src[offset+23] + 
		src[offset+24] + src[offset+25] + src[offset+26] + src[offset+27] + 
		src[offset+28] + src[offset+29] + src[offset+30] + src[offset+31];
#else
	int offset = gid << 6;
	sub_sum[gid] = 
		src[offset] + src[offset+1] + src[offset+2] + src[offset+3] + 
		src[offset+4] + src[offset+5] + src[offset+6] + src[offset+7] + 
		src[offset+8] + src[offset+9] + src[offset+10] + src[offset+11] + 
		src[offset+12] + src[offset+13] + src[offset+14] + src[offset+15] +
		src[offset+16] + src[offset+17] + src[offset+18] + src[offset+19] + 
		src[offset+20] + src[offset+21] + src[offset+22] + src[offset+23] + 
		src[offset+24] + src[offset+25] + src[offset+26] + src[offset+27] + 
		src[offset+28] + src[offset+29] + src[offset+30] + src[offset+31] +
		src[offset+32] + src[offset+33] + src[offset+34] + src[offset+35] + 
		src[offset+36] + src[offset+37] + src[offset+38] + src[offset+39] + 
		src[offset+40] + src[offset+41] + src[offset+42] + src[offset+43] + 
		src[offset+44] + src[offset+45] + src[offset+46] + src[offset+47] +
		src[offset+48] + src[offset+49] + src[offset+50] + src[offset+51] + 
		src[offset+52] + src[offset+53] + src[offset+54] + src[offset+55] + 
		src[offset+56] + src[offset+57] + src[offset+58] + src[offset+59] + 
		src[offset+60] + src[offset+61] + src[offset+62] + src[offset+63];
#endif
}
