#include <stdio.h>
#include <stdlib.h>
#include <time.h>

//#define __ARM__

#ifdef __APPLE__
#include <OpenCL/opencl.h>
#else
#include <CL/cl.h>
#endif

#define MAX_SOURCE_SIZE (0x100000)

#define CL_PRINTF_CALLBACK_ARM      0x40B0
#define CL_PRINTF_BUFFERSIZE_ARM    0x40B1

void printf_callback( const char *buffer, size_t len, size_t complete, void *user_data )
{
	printf("%.*s", len, buffer);
}

int main() {

  FILE *fp;
  char fileName[] = "./opencl_sum.cl";
  char *source_str;
  size_t source_size;
  cl_platform_id cp_platform; 
  cl_device_id *devices;

  fp = fopen(fileName, "r");
  if (!fp) {
    fprintf(stderr, "Failed to load kernel\n");
    exit(1);
  }
  source_str = (char*)malloc(MAX_SOURCE_SIZE);
  source_size = fread(source_str, 1, MAX_SOURCE_SIZE, fp);
  fclose(fp);

  cl_device_id device_id = NULL;
  cl_context context = NULL;
  cl_command_queue command_queue = NULL;
  cl_mem memobj = NULL;
  cl_program program = NULL;
  cl_kernel kernel = NULL;
  cl_platform_id* platform_id = NULL;
  cl_uint ret_num_devices;
  cl_uint ret_num_platforms;
  cl_int ret;

  char ch_buffer[1024];

  cl_ulong val[1];

  // get number of platforms
  ret = clGetPlatformIDs(0, NULL, &ret_num_platforms);
  printf(" # of platform = %u\n", ret_num_platforms);
  platform_id =
	  (cl_platform_id*)malloc(ret_num_platforms*sizeof(cl_platform_id));

  ret = clGetPlatformIDs(ret_num_platforms, platform_id, NULL);

  for (cl_uint i = 0; i < ret_num_platforms; i++) {
	  ret = clGetPlatformInfo(platform_id[i], CL_PLATFORM_NAME, 1024,
			  &ch_buffer, NULL);
	  if (CL_SUCCESS == ret) {
		  printf("platform = %s\n", ch_buffer);
	  }
  }

  cp_platform = platform_id[0];

  // get all devcies
  cl_uint target_device = 0;
  cl_uint num_compute_units;

  printf("Get the Device info and select device...\n");

  ret = clGetDeviceIDs(cp_platform, CL_DEVICE_TYPE_GPU, 0, NULL, &ret_num_devices);
  if (CL_SUCCESS != ret) {
	  printf("get device num error \n");
  }

  devices = (cl_device_id *)malloc(ret_num_devices * sizeof(cl_device_id));
  ret = clGetDeviceIDs(cp_platform, CL_DEVICE_TYPE_GPU, ret_num_devices,
		  devices, NULL);

  if (CL_SUCCESS != ret) {
	  printf("get device id error\n");
  }

  printf(" # of devices available = %u\n", ret_num_devices);

  target_device = 0;

  ret = clGetDeviceInfo(devices[target_device], CL_DEVICE_NAME,
		  sizeof(ch_buffer), &ch_buffer, NULL);

  ret = clGetDeviceInfo(devices[target_device], CL_DEVICE_MAX_COMPUTE_UNITS,
		  sizeof(num_compute_units), &num_compute_units, NULL);
  if (CL_SUCCESS != ret) {
	  printf("get num compute units error \n");
  }

  printf(" # of compute units = %u\n", num_compute_units);

  device_id = devices[target_device];
#ifdef __ARM__
	cl_context_properties properties[] =
	{
		CL_PRINTF_CALLBACK_ARM,   (cl_context_properties) printf_callback,
		CL_PRINTF_BUFFERSIZE_ARM, (cl_context_properties) 0x100000,
		CL_CONTEXT_PLATFORM,      (cl_context_properties) platform_id,
		0
	};
  context = clCreateContext(properties, 1, &device_id, NULL, NULL, &ret);
#else
  context = clCreateContext(NULL, 1, &device_id, NULL, NULL, &ret);
#endif

  //cl_ulong local_work_size = 512;
  //cl_ulong num_elements = local_work_size*64*1000*2;
  //cl_ulong local_work_size = 256;
  //cl_ulong num_elements = local_work_size*64*1000*4;
  //cl_ulong local_work_size = 128;
  //cl_ulong num_elements = local_work_size*64*1000*8;
  cl_ulong local_work_size = 64;
  cl_ulong num_elements = local_work_size*64*1000;
  //cl_ulong local_work_size = 32;
  //cl_ulong num_elements = local_work_size*64*1000*32;
 
  cl_ulong global_work_size = num_elements/64;

  cl_ulong *src, *subsum;

  src = (cl_ulong*) malloc(sizeof(cl_ulong)*num_elements);
  subsum = (cl_ulong*) malloc(sizeof(cl_ulong)*global_work_size);

  cl_ulong* cl_src = (cl_ulong*) src;

  for (cl_uint i = 0; i < num_elements; i++) {
	  cl_src[i] = i;
  }

  command_queue = clCreateCommandQueue(context, device_id, 0, &ret);

  //memobj = clCreateBuffer(context, CL_MEM_READ_WRITE, sizeof(cl_mem), NULL, &ret);
  cl_mem cm_src;
  cl_mem cm_subsum;

  cm_src = clCreateBuffer(context, CL_MEM_READ_ONLY, sizeof(cl_ulong) *
		  num_elements, NULL, &ret);

  cm_subsum = clCreateBuffer(context, CL_MEM_WRITE_ONLY, sizeof(cl_ulong) *
		  global_work_size, NULL, &ret);

  program = clCreateProgramWithSource(context, 1, (const char **)&source_str, (const size_t *)&source_size, &ret);
  ret = clBuildProgram(program, 1, &device_id, NULL, NULL, NULL);
  kernel = clCreateKernel(program, "vector_add", &ret);

  ret = clSetKernelArg(kernel, 0, sizeof(cl_mem), (void*)&cm_src);
  if (CL_SUCCESS != ret) {
	  printf("clSetKernelArg error\n");
  }

  ret = clSetKernelArg(kernel, 1, sizeof(cl_mem), (void*)&cm_subsum);
   if (CL_SUCCESS != ret) {
	  printf("clSetKernelArg error\n");
  }

  ret = clSetKernelArg(kernel, 2, sizeof(cl_ulong),
		  (void*)&num_elements);
  if (CL_SUCCESS != ret) {
	  printf("clSetKernelArg error\n");
  }

  ret = clEnqueueWriteBuffer(command_queue, cm_src, CL_FALSE, 0,
		  sizeof(cl_ulong) * num_elements, src, 0, NULL, NULL);

  if (CL_SUCCESS != ret) {
	  printf("copying failed\n");
  }

#if 0
  ret = clSetKernelArg(kernel, 0, sizeof(cl_mem), (void*)&memobj);
  if (CL_SUCCESS != ret) {
	  printf("clSetKernelArg error\n");
  }
#endif

  clock_t begin = clock();

  // 커널을 실행한다.
  printf("clEnqueueNDRangeKernel (sum)...\n");
  ret = clEnqueueNDRangeKernel(command_queue, kernel, 1, NULL,
		  &global_work_size, &local_work_size, 0, NULL, NULL);
  if (CL_SUCCESS != ret) {
	  printf("launching failed : %d\n", ret);
  }

#if 0
  ret = clEnqueueTask(command_queue, kernel, 0, NULL, NULL);
  if (CL_SUCCESS != ret) {
	  printf("clEnqueueTask error\n");
  }
#endif

#if 0
  ret = clEnqueueReadBuffer(command_queue, memobj, CL_TRUE, 0, sizeof(cl_mem), val, 0, NULL, NULL);
  if (CL_SUCCESS != ret) {
	  printf("clEnqueueReadBuffer error\n");
  }
#endif

  ret = clEnqueueReadBuffer(command_queue, cm_subsum, CL_TRUE, 0,
		  sizeof(cl_ulong)*global_work_size, subsum, 0, NULL, NULL);
  if (CL_SUCCESS != ret) {
	  printf("clEnqueueReadBuffer error\n");
  }


  cl_ulong sum = 0;
  for (cl_ulong i = 0; i < global_work_size; i++) {
	  sum += subsum[i];
	  if (i == 0)
		  printf("sum[0] = %d\n", sum);
	  if (i == global_work_size-1)
		  printf("sum[%d] = %d\n", i, sum);
  }

  clock_t end = clock();
  double runtime = (double)(end - begin) / CLOCKS_PER_SEC;

  ret = clFlush(command_queue);
  ret = clFinish(command_queue);
  ret = clReleaseKernel(kernel);
  ret = clReleaseProgram(program);
  ret = clReleaseMemObject(memobj);
  ret = clReleaseCommandQueue(command_queue);
  ret = clReleaseContext(context);

  //printf("Result: %llu\n", val[0]);
  printf("Result: %llu\n", sum);
  printf("Runtime: %lfms\n", runtime);

  free(src);
  free(subsum);
  free(source_str);

  return 0;
}
