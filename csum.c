#include <stdio.h>
#include <time.h>

int main(void)
{
	long val = 0;

	unsigned long size = 64*64*1000;
	unsigned long *src;

	src = (unsigned long*) malloc(sizeof(unsigned long)*size);

	for(long j=0; j<size; j++)
	{
		src[j] = j;
	}

	clock_t begin = clock();

	for(long j=0; j<size; j++) {
		val += src[j];
	}

	clock_t end = clock();
	printf("result  = %lld\n", val);
	printf("runtime = %lfms\n", (double)(end-begin)/CLOCKS_PER_SEC);
	return 0;
}
